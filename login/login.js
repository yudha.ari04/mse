const supertest = require('supertest');
const env = require('dotenv').config();

function USER() {
    before('login', async () => {
        var body = {
            phone: '087738190813',
            password: '12345',
            latlong: '-7.560638561378859, 110.2584650772431',
            device_token: '123',
            device_type: 0
           };
        const res = supertest(process.env.baseURL + '/api/v1/oauth/sign_in')
        .post('')
        .field(body)
        token = (await res).body.data.user.access_token
    })
}

module.exports = {USER};
const supertest = require('supertest');
const expect = require("chai").expect;
const env = require('dotenv').config();
const USER = require('../login/login')

describe('update user profile', function () {
USER.USER()
   

var education = {
    school_name: 'SMA1',
    graduation_time: '31-01-2001',
       };

it('update_education', async () => {
    const res = await supertest(process.env.baseURL + '/api/v1/profile/education')
        .post('')
        .set('Authorization', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .field(education)
        .expect(201)
    if (res.status === 201) {
        console.log(res.body.data.user.name)    
    }
    else {
        console.log('Error, check your script')
    }
       
    });
it('change_picture', async () => {
    const res = await supertest(process.env.baseURL + '/api/v1/uploads/profile')
        .post('')
        .set('Content-Type', 'multipart/form-data')
        .set('Authorization', token)
        .attach('image', './files/download.jpg')
        .expect(201)
    if (res.status === 201) {
        console.log(res.body.data.user_picture.picture.url)    
    }
    else {
        console.log('Error, check your script')
    }
       
    });




})


